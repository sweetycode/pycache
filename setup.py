import re
import ast
import os
from setuptools import setup, find_packages

with open('pycache/__init__.py') as f:
    ver = re.search('__version__\s*=\s*(.*)', f.read().decode('utf8')).group(1)
    version = str(ast.literal_eval(ver))


setup(
    name='pycache',
    version=version,
    packages=find_packages(),
    install_requires=[],
    include_package_data=True
)