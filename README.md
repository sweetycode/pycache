# python 缓存相关

## memcache

```python
#不传参数，默认永久缓存
@memcached
def test():
    pass


#与上面相同
@memcached()
def test():
    pass


#缓存30s
@memcached(30)
def test():
    pass


#与上面一样
@memcached(expire=30)
def test():
    pass


#自定义key
@memcached(key='mytest')
def test():
    pass


#自定义key可以是函数，且接收参数
@memcached(key=lambda limit: 'top_{}'.format(limit))
def top(limit):
    pass


#可以在某些条件下不走缓存逻辑
@memcached(bypass=lambda limit: limit<100)
def top(limit):
    pass


#与classmethod搭配使用
class A(object):
    @classmethod
    @memcached
    def test(cls):
        pass

```


## cached_property
```python
class A(object):
    @cached_property
    def test(self):
        pass


# 缓存30s
class A(object):
    @cached_property(30)
    def test(self):
        pass

# 自定义逻辑绕过缓存逻辑
class A(object):
    @cached_property(bypass=lambda self: getattr(self, id)==0)
    def test(self):
        pass 
```
