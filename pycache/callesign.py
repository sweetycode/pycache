#coding=utf8

#https://github.com/angadgill/Parallel-SGD/blob/master/scikit-learn/sklearn/externals/joblib/func_inspect.py


import inspect


def get_func_name(func):
    """获取函数名
    """
    module, name = '', ''

    if hasattr(func, '__module__'):
        module = func.__module__
    else:
        try:
            module = inspect.getmodule(func)
        except TypeError:
            if hasattr(func, '__class__'):
                module = func.__class__.__module__

    if not module:
        module = 'unknown'

    if inspect.ismethod(func):
        if hasattr(func, 'im_class'):
            clsname = func.im_class.__name__
        if clsname == 'type' and hasattr(func, 'im_self'):
            clsname = func.im_self.__name__
        module += '.' + clsname

    if hasattr(func, 'func_name'):
        name = func.func_name
    elif hasattr(func, '__name__'):
        name = func.__name__

    if not name:
        name = 'unknown'  

    return module + '.' + name


def format_args(func, *args, **kwargs):
    """格式化参数
    """
    argspec = inspect.getargspec(func)
    kw = {}
    if argspec.defaults:
        kw.update(dict(zip(argspec.args[-len(argspec.defaults):], argspec.defaults)))
    kw.update(dict(zip(argspec.args[:len(args)], args)))
    kw.update(kwargs)

    arglist = [kw.pop(k, '') for k in argspec.args]
    if argspec.varargs is not None and len(args)-len(argspec.args)>0:
        arglist.append('({})'.format(','.join(str(arg) for arg in args[len(argspec.args)-len(args):])))
    
    arglist.extend(['{}={}'.format(k, kw[k]) for k in sorted(kw.keys())])
    return '#'.join(str(arg) for arg in arglist)


def callesign(func, *args, **kwargs):
    return get_func_name(func) + '#' + format_args(func, *args, **kwargs)
