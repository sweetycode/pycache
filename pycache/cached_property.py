#coding=utf8
import time
import inspect


class CachedProperty(object):
    def __init__(self, func, expire=None, bypass=None):
        argspec = inspect.getargspec(func)
        assert len(argspec.args) == 1

        self.func = func
        self.__doc__ = getattr(func, '__doc__')
        self.expire = expire
        self.bypass = bypass

        self.expire_at = 0
        self.val = None

    def __get__(self, instance, cls=None):
        if instance is None:
            return self

        if self.bypass is None and self.expire is None:
            val = instance.__dict__[self.func.__name__] = self.func(instance)
            return val

        if self.bypass and self.bypass(instance):
            return self.func(instance)

        if self.expire is None:
            if self.val is None:
                self.val = self.func(instance)
            return self.val
        else:
            now = int(time.time())
            if self.expire_at <= now:
                self.val = self.func(instance)
                self.expire_at = now + self.expire
            return self.val


def cached_property(expire=None, bypass=None):
    """结果为None仍然被缓存
    """
    def wrapper(expire, bypass):
        def decorator(fn):
            return CachedProperty(fn, expire, bypass)

        return decorator

    if callable(expire):
        fn = expire
        assert bypass is None
        return wrapper(None, None)(fn)
    else:
        return wrapper(expire, bypass)
