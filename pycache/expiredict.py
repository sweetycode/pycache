#coding=utf8
import time
import bisect
import random; random.seed()


class ExpireDict(object):
    """自动过期 map
    
    支持设置key设置过期时间，到期自动删除
    """
    def __init__(self):
        """使用 dict + sorted list 实现
        
        dict 结构
            key -> (expire, value)
            expire 为 None 表示不过期

        sorted list 结构
            [(-expire, key),]
            expire 取负数，以便删除时从后向前
        """
        self.data = {}
        self.expiration = []

    def get(self, key, default=None):
        """获取值
        """
        self._random_expire()

        now = int(time.time())
        try:
            expire_at, val = self.data[key]
            if expire_at is None or expire_at > now:
                return val
        except KeyError:
            pass

        return default

    def set(self, key, val, expire_second=None):
        """更新值

        :args expire_second: 过期时间，None 表示不过期
        """
        self._random_expire()

        if expire_second is None:
            self.data[key] = (None, val)
        else:
            now = int(time.time())
            expire_at = now + expire_second
            self.data[key] = (expire_at, val)
            bisect.insort(self.expiration, (-expire_at, key))

    def _random_expire(self, percent=10):
        if percent > random.randint(0, 99):
            self._do_expire()

    def _do_expire(self):
        now = int(time.time())
        for n in xrange(len(self.expiration), 0, -1):
            idx = n-1
            n, key = self.expiration[idx]
            expire_at = -n
            if expire_at > now:
                break
            else:
                self.expiration.pop(idx)
                try:
                    expire_at, _ = self.data[key]
                    # 二次确认，避免复写情况下误删
                    if expire_at <= now:
                        del self.data[key]
                except KeyError:
                    pass
