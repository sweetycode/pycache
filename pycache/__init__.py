__VERSION__ = __version__ = '0.0.1'


from .cache import memcache_get, memcache_set, memcached
from cached_property import cached_property
