#coding=utf8

from functools import wraps, partial

from .expiredict import ExpireDict
from .callesign import callesign


_cache = ExpireDict()


def memcache_set(key, val, expire=None):
    """设置缓存
    :args key: key
    :args val: val
    :args expire: 过期时间：秒，None 表示不过期
    """
    _cache.set(key, val, expire)


def memcache_get(key, default=None, expire=None, set=True):
    """获取缓存的值

    default 结果为None时不缓存结果
    """

    val = _cache.get(key, None)
    if val is not None:
        return val

    val = default() if callable(default) else default
    if val is not None and set:
        memcache_set(key, val, expire)

    return val


def memcached(expire=None, key=None, bypass=None):
    """装饰器
    
    结果为None时不被缓存

    usage:
        @memcached
        def func():
            pass

        @memcached()
        def func():
            pass

        @memcached(3600)
        def func():
            pass

        @memcached(expire=3600)
        def func():
            pass

        @memcached(key='123', bypass=lambda *args, **kw: False)
        def func():
            pass

        @memcached(key=lambda *args, **kw: '123')
        def func():
            pass
    """

    def wrapper(expire_, key_, bypass_):
        def decorator(fn):
            @wraps(fn)
            def decorated(*args, **kwargs):
                expire, key, bypass = expire_, key_, bypass_

                if bypass is not None and bypass(*args, **kwargs):
                    return fn(*args, **kwargs)

                if isinstance(key, basestring):
                    pass
                elif callable(key):
                    key = key(*args, **kwargs)
                elif key is None:
                    key = callesign(fn, *args, **kwargs)

                val = memcache_get(key, None)
                if val is None:
                    val = fn(*args, **kwargs)
                    if val is not None:
                        memcache_set(key, val, expire)
                
                return val

            return decorated
        return decorator

    if callable(expire):
        fn = expire
        assert key is None and bypass is None
        return wrapper(None, None, None)(fn)
    else:
        return wrapper(expire, key, bypass)
